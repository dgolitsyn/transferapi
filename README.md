Money transferring service
=============================

Provide simple rest api service to manage money transfers between user accounts.

### Getting started ###

Building with maven: 

```
#!python

mvn clean install
```

Starting the server:

```
#!python

mvn exec:java -Dexec.args="${port}"  // default port is 8081
```

Rest api should be available at localhost:8081/api/v1/. 
You can find api methods documentation [here](https://bitbucket.org/dgolitsyn/transferapi/src/master/src/main/resources/doc/doc.json?at=master&fileviewer=file-view-default)

Or better just **[TRY IT OUT](http://178.62.226.7:8081/docs/#/)**


### Dependencies ###

Required:

* Jersey (JAX-RS)

* Grizzly (NIO, really lightweight and easy)

* Jackson (JSON support)

* Guava   (collections, caches)

Optional:

* Swagger (ui for api)

### Overview ###

The domain model is really simple and contains only three classes:

* User (userId, accounts)

* Account (accountId, currency, amount)

* Transaction (transactionId, accountFrom, accountTo, amount, state)

All data stored in DataStore. DataStore operates with Entity objects - base class for all domain models. Entity contains Key (unique identifier) and revision to prevent stale object updates in datastore. 
LockManager interface is used in DataStore to avoid concurrent modifications of domain objects. 

Money transfers are represented as a Transactions and are processed by a TransactionProcessor. Several concurrent control strategies could be used:

* Optimistic concurrent control strategy - do not lock any resources while process transaction (assumes that most transactions has no data intersections and thus in most cases locking will be a huge overhead). If multiple transactions on the same resources are running in parallel - both of them would be cancelled in the worst case. Or only one of them if the order of money transferring is the same. 

* Pessimistic concurrent control strategy - lock resources to be update by the transaction. 

LockManager is used while rollback cancelled transactions (to guarantee success of the rollback operation we lock all dependent resources and rollback changes only after that).

During transaction processing currency is converted from accountFrom's currency to accountTo's currency. CurrencyConverter and RatesProvider interfaces are used for this purposes. RatesProvider using [simple exchange api](http://api.fixer.io/latest) to keep rates always up to date.

### Architecture ###

Current implementation uses only local objects (LocalDataStore, LocalLockManager) and thus has number of limitations:

* is not distributed

* no dr failover

* no data persistency

Service is built on top of number of simple interfaces and could be easily extended to fully distributed service by providing list of implementations:

* DistributedDataStore

* DistributedLockManager

For example it could be MongoDataStore. MongoDB is distributed, replicated and in-memory which leads to good perfomance resulsts and adequate stability.
As a DistributedLock one could use memcache (really simple, stable and fast enough).

### TODO ###

* **tests**
* logging
* exception handling
* responce statuses