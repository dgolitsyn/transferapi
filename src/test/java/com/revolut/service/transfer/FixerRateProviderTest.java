package com.revolut.service.transfer;

import com.revolut.service.transfer.currency.FixerRatesProvider;
import com.revolut.service.transfer.currency.RatesProvider;

public class FixerRateProviderTest {

    public static void main(String[] args) {
        RatesProvider ratesProvider = new FixerRatesProvider();
        System.out.println(ratesProvider.rate("EUR", "USD"));
        System.out.println(ratesProvider.rate("USD", "EUR"));
        System.out.println(ratesProvider.rate("USD", "GBP"));
        System.out.println(ratesProvider.rate("GBP", "USD"));
    }
}
