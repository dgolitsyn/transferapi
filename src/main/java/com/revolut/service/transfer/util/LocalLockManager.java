package com.revolut.service.transfer.util;

import com.google.common.collect.Maps;
import com.revolut.service.transfer.datastore.Key;

import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class LocalLockManager implements LockManager {

    private final int locksNumber;
    private final Map<Integer, ReentrantLock> locks;

    public LocalLockManager(int locksNumber) {
        this.locksNumber = locksNumber;
        this.locks = Maps.newHashMapWithExpectedSize(locksNumber);
        IntStream.range(0, locksNumber).forEach(i -> locks.put(i, new ReentrantLock()));
    }

    @Override
    public void lock(Key key) {
        locks.get(getLockNumber(key)).lock();
    }

    @Override
    public void unlock(Key key) {
        locks.get(getLockNumber(key)).unlock();
    }

    private int getLockNumber(Key key) {
        return Math.abs(key.hashCode() % locksNumber);
    }
}
