package com.revolut.service.transfer.util;

import com.revolut.service.transfer.datastore.Key;

public interface LockManager {

    void lock(Key key);

    void unlock(Key key);

}
