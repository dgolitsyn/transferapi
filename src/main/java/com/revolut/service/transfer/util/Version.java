package com.revolut.service.transfer.util;

public class Version {

    private static final Version INSTANCE = new Version();

    public static Version getVersion() {
        return INSTANCE;
    }

    private String apiVersion;

    private Version() {
        //TODO: read from build.info
        apiVersion = "v1";
    }

    public String getApiVersion() {
        return apiVersion;
    }
}
