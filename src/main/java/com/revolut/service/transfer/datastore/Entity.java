package com.revolut.service.transfer.datastore;


import com.fasterxml.jackson.annotation.JsonView;
import com.revolut.service.transfer.resource.Views;


public class Entity<T> {

    private final Key<T> key;
    private long revision;

    public Entity(Key<T> key) {
        this(key, 0);
    }

    public Entity(Key<T> key, long revision) {
        this.key = key;
        this.revision = revision;
    }

    public Entity(Entity<T> other) {
        this.key = other.key;
        this.revision = other.revision + 1;
    }

    public String getId() {
        return key.getId();
    }

    @JsonView(Views.ExtendedEntityView.class)
    public Key getKey() {
        return key;
    }

    public long getRevision() {
        return revision;
    }

    public void setRevision(long revision) {
        this.revision = revision;
    }
}
