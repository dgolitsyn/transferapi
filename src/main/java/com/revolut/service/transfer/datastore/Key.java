package com.revolut.service.transfer.datastore;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;


import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class Key<T> implements Comparable<Key<T>> {
    public static final String MAX_ID = StringUtils.repeat(Character.MAX_VALUE, 1000);

    private final String id;
    private final Class<T> type;

    public static Key createKey(String id, Class type) {
        return new Key(id, type);
    }

    public Key(String id, Class type) {
        this.id = checkNotNull(id);
        this.type = checkNotNull(type);
        checkArgument(MAX_ID.compareTo(id) >= 0);
    }

    public String getId() {
        return id;
    }

    public Class getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Key key = (Key) o;

        if (!id.equals(key.id)) return false;
        if (!type.equals(key.type)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("type", type)
                .toString();
    }

    @Override
    public int compareTo(Key<T> o) {
        if (type.equals(o.getType())) {
            return id.compareTo(o.getId());
        } else {
            return type.getName().compareTo(o.getType().getName());
        }
    }
}
