package com.revolut.service.transfer.datastore;


import com.revolut.service.transfer.exception.RevisionException;
import com.revolut.service.transfer.util.LockManager;

import java.util.Collection;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

public class LocalDataStore implements DataStore {

    ConcurrentSkipListMap<Key, Entity> entities = new ConcurrentSkipListMap<>();
    LockManager lockManager;

    public LocalDataStore(LockManager lockManager) {
        this.lockManager = lockManager;
    }

    @Override
    public <T> Collection<T> getAll(Class type) {
        return entities.subMap(
                Key.createKey("", type), true,
                Key.createKey(Key.MAX_ID, type), false
                ).values().stream().map(entity -> (T) entity).collect(Collectors.toList());
    }

    @Override
    public Entity get(Key key) {
        return entities.get(key);
    }

    @Override
    public void update(Entity entity) {
        lockManager.lock(entity.getKey());
        try {
            Entity existedEntity = entities.get(entity.getKey());
            if (existedEntity == null || entity.getRevision() > existedEntity.getRevision()) {
                entities.put(entity.getKey(), entity);
            } else {
                throw new RevisionException("Failed to update entity - revision is outdated");
            }
        } finally {
            lockManager.unlock(entity.getKey());
        }
    }

    @Override
    public Entity delete(Key key) {
        return entities.remove(key);
    }

    @Override
    public long getRevision(Key key) {
        Entity entity = get(key);
        return (entity == null) ? 0 : entity.getRevision();
    }
}
