package com.revolut.service.transfer.datastore;


import java.util.Collection;

public interface DataStore {

    <T> Collection<T> getAll(Class type);

    <T> T get(Key key);

    void update(Entity entity);

    <T> T delete(Key key);

    long getRevision(Key key);

}
