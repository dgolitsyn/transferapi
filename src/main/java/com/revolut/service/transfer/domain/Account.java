package com.revolut.service.transfer.domain;

import com.revolut.service.transfer.datastore.Entity;
import com.revolut.service.transfer.datastore.Key;

public class Account extends Entity {

    private final String currency;

    private final double amount;

    public static Key createKey(String id) {
        return Key.createKey(id, Account.class);
    }

    public Account(String id, String currency, double amount, long revision) {
        super(new Key(id, Account.class), revision);
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("Failed to create account - id can't be null or empty");
        }
        if (currency == null || currency.isEmpty()) {
            throw new IllegalArgumentException("Failed to create account - currency can't be null or empty");
        }
        this.currency = currency;
        this.amount = amount;
    }

    public Account(Account other) {
        super(other);
        this.currency = other.currency;
        this.amount = other.amount;
    }

    public String getCurrency() {
        return currency;
    }

    public double getAmount() {
        return amount;
    }

    public Account copy(double amount, long revision) {
        return new Account(getId(), currency, amount, revision);
    }

    @Override
    public String toString() {
        return "Account{ " + super.toString() +
                " currency='" + currency + '\'' +
                ", amount=" + amount +
                '}';
    }
}
