package com.revolut.service.transfer.domain;

import com.google.common.collect.Sets;
import com.revolut.service.transfer.datastore.Entity;
import com.revolut.service.transfer.datastore.Key;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

public class User extends Entity {

    private final Set<String> accounts;

    public static Key createKey(String id) {
        return Key.createKey(id, User.class);
    }

    public User(String id, Set<String> accounts, long revision) {
        super(Key.createKey(id, User.class), revision);
        this.accounts = accounts;
    }

    public Set<String> getAccounts() {
        return accounts;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .appendSuper(super.toString())
                .append("accounts", accounts)
                .toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String id;
        private long revision = 0;
        private Set<String> accounts = Sets.newHashSet();

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withRevision(long revision) {
            this.revision = revision;
            return this;
        }

        public Builder addAccount(Account account) {
            accounts.add(account.getKey().getId());
            return this;
        }

        public Builder addAccount(String accountId) {
            accounts.add(accountId);
            return this;
        }

        public User build() {
            checkNotNull(id, "Failed to create user - id can't be null");
            return new User(id, accounts, revision);
        }

    }

}
