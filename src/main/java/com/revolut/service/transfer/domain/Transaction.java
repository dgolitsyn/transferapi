package com.revolut.service.transfer.domain;

import com.revolut.service.transfer.datastore.Entity;
import com.revolut.service.transfer.datastore.Key;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Transaction extends Entity {

    public enum State {
        PENDING,
        CANCELLED,
        COMMITTED
    }

    private final String accountFrom;

    private final String accountTo;

    private final double amount;

    private volatile State state;

    public static Key createKey(String id) {
        return Key.createKey(id, Transaction.class);
    }

    public Transaction(String id, String accountFrom, String accountTo, double amount, State state, long revision) {
        super(Key.createKey(id, Transaction.class), revision);
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amount = amount;
        this.state = state;
    }

    public void cancel() {
        if (state == State.PENDING) {
            state = State.CANCELLED;
            return;
        }
        throw new IllegalStateException("Failed to cancel transaction - not in pending state");
    }

    public void commit() {
        if (state == State.PENDING) {
            state = State.COMMITTED;
            return;
        }
        throw new IllegalStateException("Failed to commit transaction - not in pending state");
    }

    public State getState() {
        return state;
    }

    public String getAccountFrom() {
        return accountFrom;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("accountFrom", accountFrom)
                .append("accountTo", accountTo)
                .append("amount", amount)
                .toString();
    }
}
