package com.revolut.service.transfer.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.collect.Sets;
import com.revolut.service.transfer.domain.Account;
import com.revolut.service.transfer.domain.Transaction;
import com.revolut.service.transfer.domain.User;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class DomainDeserializerModule extends SimpleModule {

    public DomainDeserializerModule() {
        super("DomainDeserializerModule");
        this.addDeserializer(User.class, new UserDeserializer());
        this.addDeserializer(Account.class, new AccountDeserializer());
        this.addDeserializer(Transaction.class, new TransactionDeserializer());
    }

    public class UserDeserializer extends JsonDeserializer<User> {
        @Override
        public User deserialize(JsonParser jp, DeserializationContext ctx) throws IOException, JsonProcessingException {
            JsonNode node = jp.getCodec().readTree(jp);
            Set<String> accounts = Sets.newHashSet();
            node.get("accounts").elements().forEachRemaining(elem -> accounts.add(elem.asText()));
            return new User(parseId(node), accounts, parseRevision(node));
        }
    }

    public class AccountDeserializer extends JsonDeserializer<Account> {
        @Override
        public Account deserialize(JsonParser jp, DeserializationContext ctx) throws IOException, JsonProcessingException {
            JsonNode node = jp.getCodec().readTree(jp);
            String currency = node.get("currency").asText();
            double amount = node.get("amount").asDouble();
            return new Account(parseId(node), currency, amount, parseRevision(node));
        }
    }

    public class TransactionDeserializer extends JsonDeserializer<Transaction> {
        @Override
        public Transaction deserialize(JsonParser jp, DeserializationContext ctx) throws IOException, JsonProcessingException {
            JsonNode node = jp.getCodec().readTree(jp);
            String accountFrom = node.get("accountFrom").asText();
            String accountTo = node.get("accountTo").asText();
            double amount = node.get("amount").asDouble();
            return new Transaction(UUID.randomUUID().toString(), accountFrom, accountTo, amount, Transaction.State.PENDING, 0);
        }
    }

    private String parseId(JsonNode node) {
        return node.get("id").asText();
    }

    private long parseRevision(JsonNode node) {
        return node.get("revision").asInt();
    }
}
