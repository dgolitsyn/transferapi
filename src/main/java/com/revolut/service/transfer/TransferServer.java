package com.revolut.service.transfer;

import com.revolut.service.transfer.configuration.ServerBinder;
import com.revolut.service.transfer.util.Version;
import io.swagger.jaxrs.config.BeanConfig;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TransferServer {
    private static final Logger log = Logger.getLogger(TransferServer.class.getName());

    private static final String BASE_URI = "http://0.0.0.0:%d/api/%s";

    private static ResourceConfig createResourceConfig() {
        ResourceConfig resourceConfig = new ResourceConfig()
                .register(ServerBinder.createDefault())
                .register(new JacksonFeature())
                .packages("com.revolut.service.transfer")
                .packages("io.swagger.jaxrs.listing");
        BeanConfig config = new BeanConfig();
        config.setResourcePackage("com.db.revolut.service.transfer");
        config.setVersion(Version.getVersion().getApiVersion());
        config.setScan(true);
        return resourceConfig;
    }

    public static HttpServer createHttpServer(int port, String apiVersion) {
        URI uri = URI.create(String.format(BASE_URI, port, apiVersion));
        HttpServer server = GrizzlyHttpServerFactory.createHttpServer(uri, createResourceConfig(), false);
        return server;
    }

    private static int parsePort(String[] args) {
        if (args != null && args.length > 0) {
            return Integer.parseInt(args[0]);
        }
        return 8081;
    }

    public static void main(String[] args) {
        log.info("Starting transfer server");
        try {
            HttpServer server = createHttpServer(parsePort(args), Version.getVersion().getApiVersion());
            Runtime.getRuntime().addShutdownHook(new Thread(() -> server.shutdownNow()));
            server.start();
            server.getServerConfiguration().addHttpHandler(
                    new CLStaticHttpHandler(TransferServer.class.getClassLoader(), "doc/"), "/docs"
            );
            Thread.currentThread().join();
        } catch (IOException | InterruptedException e) {
            log.log(Level.SEVERE, "Server failed : exception occured", e);
        }
    }
}