package com.revolut.service.transfer.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.service.transfer.serialization.DomainDeserializerModule;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
public class JacksonObjectMapperProvider implements ContextResolver<ObjectMapper> {

final ObjectMapper defaultObjectMapper;

public JacksonObjectMapperProvider() {
        defaultObjectMapper = createDefaultMapper();
}

@Override
public ObjectMapper getContext(Class<?> type) {
        return defaultObjectMapper;
}

private static ObjectMapper createDefaultMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new DomainDeserializerModule());
        return mapper;
        }
}
