package com.revolut.service.transfer.configuration;

import com.revolut.service.transfer.currency.CurrencyConverter;
import com.revolut.service.transfer.currency.FixerRatesProvider;
import com.revolut.service.transfer.currency.RatesProvider;
import com.revolut.service.transfer.datastore.DataStore;
import com.revolut.service.transfer.datastore.LocalDataStore;
import com.revolut.service.transfer.transaction.OptimisticConcurrencyControlStrategy;
import com.revolut.service.transfer.transaction.TransactionProcessor;
import com.revolut.service.transfer.transaction.TransactionProcessorImpl;
import com.revolut.service.transfer.util.LocalLockManager;
import com.revolut.service.transfer.util.LockManager;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

import javax.ws.rs.ext.Provider;
import java.util.Properties;

@Provider
public class ServerBinder extends AbstractBinder {

    private DataStore dataStore;

    private TransactionProcessor transactionProcessor;

    public static ServerBinder createDefault() {
        LockManager lockManager = new LocalLockManager(10);
        DataStore dataStore1 = new LocalDataStore(lockManager);
        RatesProvider ratesProvider = new FixerRatesProvider();
        CurrencyConverter currencyConverter = new CurrencyConverter(ratesProvider);
        TransactionProcessor transactionProcessor1 = new TransactionProcessorImpl(
                dataStore1,
                new OptimisticConcurrencyControlStrategy(),
                lockManager,
                currencyConverter);
        return new ServerBinder(dataStore1, transactionProcessor1);
    }

    public ServerBinder(Properties properties) {
        //TODO: create beans with configuration properties
    }

    public ServerBinder(DataStore dataStore, TransactionProcessor transactionProcessor) {
        this.dataStore = dataStore;
        this.transactionProcessor = transactionProcessor;
    }

    @Override
    protected void configure() {
        bind(dataStore).to(DataStore.class);
        bind(transactionProcessor).to(TransactionProcessor.class);
    }
}
