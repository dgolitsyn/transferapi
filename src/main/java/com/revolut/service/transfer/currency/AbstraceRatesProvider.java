package com.revolut.service.transfer.currency;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.TimeUnit;

public abstract class AbstraceRatesProvider implements RatesProvider {

    private static final String BASE_CURRENCY = "EUR";

    protected abstract double fetchEurRate(String currency);

    private final LoadingCache<String, Double> eurRatesCache = CacheBuilder
            .newBuilder()
            .refreshAfterWrite(10, TimeUnit.MINUTES)
            .expireAfterAccess(15, TimeUnit.MINUTES)
            .build(new CacheLoader<String, Double>() {
                @Override
                public Double load(String key) throws Exception {
                    return fetchEurRate(key);
                }
            });

    @Override
    public double rate(String sellCurrency, String buyCurrency) {
        try {
            if (sellCurrency.trim().toUpperCase().equals(BASE_CURRENCY)) {
                return eurRatesCache.get(buyCurrency);
            }
            if (buyCurrency.trim().toUpperCase().equals(BASE_CURRENCY)) {
                return 1 / eurRatesCache.get(sellCurrency);
            }
            return eurRatesCache.get(buyCurrency) / eurRatesCache.get(sellCurrency);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

