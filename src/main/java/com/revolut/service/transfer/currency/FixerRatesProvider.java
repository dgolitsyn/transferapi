package com.revolut.service.transfer.currency;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FixerRatesProvider extends AbstraceRatesProvider {

    private static final String URL_FORMAT = "http://api.fixer.io/latest?symbols=%s";

    @Override
    protected double fetchEurRate(String currency) {
        try {
            URL url = new URL(String.format(URL_FORMAT, currency.trim().toUpperCase()));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(response.toString());
            if (node != null) {
                return node.get("rates").elements().next().asDouble();
            }
            throw new RuntimeException("Failed to fetch rate for currency" + currency);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
