package com.revolut.service.transfer.currency;

public class CurrencyConverter {

    private RatesProvider ratesProvider;

    public CurrencyConverter(RatesProvider ratesProvider) {
        this.ratesProvider = ratesProvider;
    }

    public double convert(double amount, String sellCurrency, String buyCurrency) {
        return amount * ratesProvider.rate(sellCurrency, buyCurrency);
    }
}
