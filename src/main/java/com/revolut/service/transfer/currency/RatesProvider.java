package com.revolut.service.transfer.currency;

public interface RatesProvider {

    public double rate(String sellCurrency, String buyCurrency);
}
