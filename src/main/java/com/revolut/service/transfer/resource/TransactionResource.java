package com.revolut.service.transfer.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.revolut.service.transfer.datastore.DataStore;
import com.revolut.service.transfer.domain.Transaction;
import com.revolut.service.transfer.transaction.TransactionProcessor;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;


@Path("transactions")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource {

    @Inject
    DataStore dataStore;

    @Inject
    TransactionProcessor transactionProcessor;

    @PathParam("{id}")
    private String id;

    @GET
    @JsonView(Views.TransactionView.class)
    public Collection<Transaction> getTransactions() {
        return dataStore.getAll(Transaction.class);
    }

    @GET
    @Path("{id}")
    @JsonView(Views.TransactionView.class)
    public Transaction getTransaction() {
        return dataStore.get(Transaction.createKey(id));
    }

    @POST
    public Transaction createTransaction(Transaction transaction) {
        dataStore.update(transaction);
        transactionProcessor.process(transaction);
        return dataStore.get(transaction.getKey());
    }

    @DELETE
    @Path("{id}")
    public void deleteTransaction() {
        dataStore.delete(Transaction.createKey(id));
    }


}
