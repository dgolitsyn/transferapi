package com.revolut.service.transfer.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.revolut.service.transfer.datastore.DataStore;
import com.revolut.service.transfer.domain.User;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;


@Path("users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    DataStore dataStore;

    @PathParam("id")
    private String id;

    @GET
    @JsonView(Views.UserView.class)
    public Collection<User> getUsers() {
        return dataStore.getAll(User.class);
    }

    @GET
    @Path("{id}")
    @JsonView(Views.UserView.class)
    public User getUser() {
        return dataStore.get(User.createKey(id));
    }

    @POST
    @Path("/update")
    public User updateUser(User user) {
        dataStore.update(user);
        return user;
    }

    @DELETE
    @Path("{id}")
    public User deleteUser() {
        return dataStore.delete(User.createKey(id));
    }

}
