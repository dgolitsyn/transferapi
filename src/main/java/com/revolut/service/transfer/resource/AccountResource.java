package com.revolut.service.transfer.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.revolut.service.transfer.datastore.DataStore;
import com.revolut.service.transfer.domain.Account;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;


@Path("accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    @Inject
    DataStore dataStore;

    @PathParam("id")
    private String id;

    @GET
    @JsonView(Views.AccountView.class)
    public Collection<Account> getAccounts() {
        return dataStore.getAll(Account.class);
    }

    @GET
    @Path("{id}")
    @JsonView(Views.AccountView.class)
    public Account getAccount() {
        return dataStore.get(Account.createKey(id));
    }

    @POST
    @Path("/update")
    public Account updateAccount(Account account) {
        dataStore.update(account);
        return account;
    }

    @DELETE
    @Path("{id}")
    public Account deleteAccount() {
        return dataStore.delete(Account.createKey(id));
    }

}
