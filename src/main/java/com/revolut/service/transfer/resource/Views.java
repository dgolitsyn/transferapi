package com.revolut.service.transfer.resource;

public class Views {

    public static class EntityView {}
    public static class UserView {}
    public static class AccountView {}
    public static class TransactionView {}

    public static class ExtendedEntityView {}
}
