package com.revolut.service.transfer.exception;

public class RevisionException extends RuntimeException {
    public RevisionException(String message) {
        super(message);
    }
}
