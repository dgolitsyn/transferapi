package com.revolut.service.transfer.transaction;

import com.revolut.service.transfer.datastore.Key;
import com.revolut.service.transfer.domain.Account;
import com.revolut.service.transfer.domain.Transaction;
import com.revolut.service.transfer.util.LockManager;

public class PessimisticConcurrencyControlStrategy implements ConcurrencyControlStrategy {

    private LockManager lockManager;

    public PessimisticConcurrencyControlStrategy(LockManager lockManager) {
        this.lockManager = lockManager;
    }

    @Override
    public void lockResources(Transaction transaction) {
        lockManager.lock(Key.createKey(transaction.getAccountFrom(), Account.class));
        lockManager.lock(Key.createKey(transaction.getAccountTo(), Account.class));
    }

    @Override
    public void unlockResources(Transaction transaction) {
        lockManager.unlock(Key.createKey(transaction.getAccountFrom(), Account.class));
        lockManager.unlock(Key.createKey(transaction.getAccountTo(), Account.class));
    }
}
