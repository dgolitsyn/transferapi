package com.revolut.service.transfer.transaction;

import com.revolut.service.transfer.domain.Transaction;

public interface TransactionProcessor {

    void process(Transaction transaction);

}
