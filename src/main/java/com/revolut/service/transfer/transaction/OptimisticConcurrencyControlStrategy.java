package com.revolut.service.transfer.transaction;

import com.revolut.service.transfer.domain.Transaction;

public class OptimisticConcurrencyControlStrategy implements ConcurrencyControlStrategy {

    @Override
    public void lockResources(Transaction transaction) {
        // do nothing (assuming transactions are usually executed without interfering)
    }

    @Override
    public void unlockResources(Transaction transaction) {

    }
}
