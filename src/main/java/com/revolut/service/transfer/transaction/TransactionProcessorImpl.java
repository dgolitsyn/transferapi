package com.revolut.service.transfer.transaction;

import com.revolut.service.transfer.currency.CurrencyConverter;
import com.revolut.service.transfer.datastore.DataStore;
import com.revolut.service.transfer.domain.Account;
import com.revolut.service.transfer.domain.Transaction;
import com.revolut.service.transfer.exception.RevisionException;
import com.revolut.service.transfer.util.LockManager;


public class TransactionProcessorImpl implements TransactionProcessor {

    private DataStore dataStore;

    private ConcurrencyControlStrategy strategy;

    private LockManager lockManager;

    private CurrencyConverter currencyConverter;

    public TransactionProcessorImpl(DataStore dataStore, ConcurrencyControlStrategy strategy, LockManager lockManager, CurrencyConverter currencyConverter) {
        this.dataStore = dataStore;
        this.strategy = strategy;
        this.lockManager = lockManager;
        this.currencyConverter = currencyConverter;
    }

    @Override
    public void process(Transaction transaction) {
        strategy.lockResources(transaction);
        try {
            if (apply(transaction)) {
                transactionSucceeded(transaction);
            } else {
                transactionFailed(transaction);
            }
            saveTransaction(transaction);
        } finally {
            strategy.unlockResources(transaction);
        }
    }

    private boolean apply(Transaction transaction) {
        final Account accountFrom = dataStore.get(Account.createKey(transaction.getAccountFrom()));
        final Account accountTo = dataStore.get(Account.createKey(transaction.getAccountTo()));

        if (!validateTransaction(transaction, accountFrom)) {
            return false;
        }

        final double amountSent = transaction.getAmount();
        final double amountReceived = currencyConverter.convert(amountSent, accountFrom.getCurrency(), accountTo.getCurrency());

        try {
            //TODO: accountFrom.toBuilder().minusAmount(amountSent).setRevision(revision + 1).build();
            dataStore.update(accountFrom.copy(accountFrom.getAmount() - amountSent, accountFrom.getRevision() + 1));
        } catch (RevisionException e) {
            return false;
        }

        try {
            dataStore.update(accountTo.copy(accountTo.getAmount() + amountReceived, accountTo.getRevision() + 1));
        } catch (RevisionException e) {
            // rollback accountFrom;
            // we need lock here because we want to ensure that rollback will be in place
            // no matter what concurrency control strategy is, (optimistic could not guarantee success of this operation)
            lockManager.lock(accountFrom.getKey());
            try {
                Account rolledBackAccount = dataStore.get(accountFrom.getKey());
                dataStore.update(rolledBackAccount.copy(rolledBackAccount.getAmount() + amountSent, rolledBackAccount.getRevision() + 1));
            } finally {
                lockManager.unlock(accountFrom.getKey());
            }
            return false;
        }
        return true;
    }

    private void saveTransaction(Transaction transaction) {
        transaction.setRevision(transaction.getRevision() + 1);
        dataStore.update(transaction);
    }

    private boolean validateTransaction(Transaction transaction, Account accountFrom) {
        if (accountFrom.getAmount() < transaction.getAmount()) {
            transactionFailed(transaction);
            return false;
        }
        return true;
    }

    private void transactionSucceeded(Transaction transaction) {
        transaction.commit();
    }

    private void transactionFailed(Transaction transaction) {
        transaction.cancel();
    }

}
