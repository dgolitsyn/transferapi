package com.revolut.service.transfer.transaction;

import com.revolut.service.transfer.domain.Transaction;

public interface ConcurrencyControlStrategy {

    void lockResources(Transaction transaction);

    void unlockResources(Transaction transaction);

}
